"use strict";

let a = prompt("fill a number");
let b;

if (a % 5 !== 0) {
  console.log("Sorry, no numbers");
} else {
  for (b = 5; b <= a; b += 5) {
    console.log(b);
  }
}
